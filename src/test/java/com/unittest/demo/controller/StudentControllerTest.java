package com.unittest.demo.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unittest.demo.model.Student;
import com.unittest.demo.repository.StudentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
        MediaType.APPLICATION_JSON.getType(),
        MediaType.APPLICATION_JSON.getSubtype(),
        Charset.forName("utf8")
    );

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentRepository studentRepositoryMock;

    @InjectMocks
    private StudentController studentController;

    @Before
    public void init () {
    }


    @Test
    public void findAll_StudentsFound_ShouldReturnFoundStudentEntries () throws Exception {
        Student first  = new Student(1l, "Bob", "A1234567");
        Student second = new Student(2l, "Alice", "B1234568");

        when(studentRepositoryMock.findAll()).thenReturn(Arrays.asList(first, second));

        mockMvc.perform(get("/student"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(2)))
               .andExpect(jsonPath("$[0].id", is(1)))
               .andExpect(jsonPath("$[0].name", is("Bob")))
               .andExpect(jsonPath("$[0].passportNumber", is("A1234567")))
               .andExpect(jsonPath("$[1].id", is(2)))
               .andExpect(jsonPath("$[1].name", is("Alice")))
               .andExpect(jsonPath("$[1].passportNumber", is("B1234568")));

        verify(studentRepositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void findById_StudentFound_ShouldReturnFoundStudentEntry () throws Exception {
        Student first  = new Student(1l, "Bob", "A1234567");
        Optional<Student> firstStudent = Optional.of(first);

        when(studentRepositoryMock.findById(1l)).thenReturn(firstStudent);

        mockMvc.perform(get("/student/1"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.name", is("Bob")))
               .andExpect(jsonPath("$.passportNumber", is("A1234567")));

        verify(studentRepositoryMock, times(1)).findById(1l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void findById_StudentNotFound_ShouldReturnNotFound () throws Exception {
        Optional<Student> notPresentStudent = Optional.empty();

        when(studentRepositoryMock.findById(3l)).thenReturn(notPresentStudent);

        mockMvc.perform(get("/student/3")).andExpect(status().isNotFound());

    }
    
    @Test
    public void deleteValidStudentById_ShouldReturnOK () throws Exception {

        mockMvc.perform(delete("/student/1"))
               .andExpect(status().isOk());

        verify(studentRepositoryMock, times(1)).deleteById(1l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void addStudent_ShouldReturnCreated () throws Exception {
    	Student newStudent = new Student(3l, "ddd", "C1234568");
    	String studentJson = asJsonString(newStudent);
    	
    	when(studentRepositoryMock.save(any(Student.class))).thenReturn(newStudent);

        mockMvc.perform(post("/student").contentType(MediaType.APPLICATION_JSON).content(studentJson))
               .andExpect(status().isCreated());

        verify(studentRepositoryMock, times(1)).save(any(Student.class));
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void updateValidStudent_ShouldReturnUpdatedStudent () throws Exception {
    	Student updatedStudent = new Student(0l, "BobUpdated", "C1234568Updated");
    	Optional<Student> secondStudent = Optional.of(updatedStudent);
    	String studentJson = asJsonString(updatedStudent);
    	
    	when(studentRepositoryMock.findById(2l)).thenReturn(secondStudent);
    	when(studentRepositoryMock.save(any(Student.class))).thenReturn(updatedStudent);

        mockMvc.perform(put("/student/2").contentType(MediaType.APPLICATION_JSON).content(studentJson))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id", is(2)))
               .andExpect(jsonPath("$.name", is("BobUpdated")))
               .andExpect(jsonPath("$.passportNumber", is("C1234568Updated")));

        verify(studentRepositoryMock, times(1)).save(any(Student.class));
        verify(studentRepositoryMock, times(1)).findById(2l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void updateInvalidStudent_ShouldReturnNotFound () throws Exception {
    	Student updatedStudent = new Student(0l, "BobUpdated", "C1234568Updated");
    	Optional<Student> notPresentStudent = Optional.empty();
    	String studentJson = asJsonString(updatedStudent);
    	
    	when(studentRepositoryMock.findById(2l)).thenReturn(notPresentStudent);

        mockMvc.perform(put("/student/3").contentType(MediaType.APPLICATION_JSON).content(studentJson))
               .andExpect(status().isNotFound());

        verify(studentRepositoryMock, times(1)).findById(3l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }

    public static String asJsonString (final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

